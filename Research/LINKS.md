# ARTICLES

How Computer Vision Technology in Our Mobile Body Measurement App Tackles Perfect Clothing Fit
https://www.abtosoftware.com/blog/computer-vision-technology-body-measurement-perfect-clothing-fit

Calculate Distance or Size of an Object in a photo image
https://www.scantips.com/lights/subjectdistance.html

Measuring distance between objects in an image with OpenCV
https://www.pyimagesearch.com/2016/04/04/measuring-distance-between-objects-in-an-image-with-opencv/

Object identification for computer vision using image segmentation
https://ieeexplore.ieee.org/document/5529412